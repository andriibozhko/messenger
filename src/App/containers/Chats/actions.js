import { SET_NEW_USER } from './constants';

export const setNewUser = (payload) => ({
  type: setNewUser,
  payload
});