import React from 'react';
import { ScrollView } from 'react-native';
import ListItem from '../../components/ListItem'

const users = [
  {
    name: 'Johny',
    surname: 'Ive',
  },
  {
    name: 'Steave',
    surname: 'Jobs',
  },
  {
    name: 'Andrii',
    surname: 'Bozhko',
  },
]

const Chats = () => {
  return (
    <ScrollView>
      {users.map((user, index) => (
        <ListItem key={index} user={user}/>
      ))}
      
    </ScrollView>
  )
}

export default Chats;