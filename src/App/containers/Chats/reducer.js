import {
  SET_NEW_USER,
} from './constants';

export const initialState = {
  user: {
    name: null,
    surname: null
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NEW_USER: 
      return {
        ...state,
        user: action.payload
      }
    default:
      return state;
  }
};
