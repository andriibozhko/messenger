import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Home from './screens/Home'
import Login from './screens/Login'
import Chat from './screens/Chat'

const headerConfig = {
   navigationBarStyle: {
      height: 52,
   }
}

const Routes = () => {
   return (
      <Router>
         <Scene key="root">
            <Scene key="login" component={Login} hideNavBar={true} />
            <Scene title='Messages' {...headerConfig} key="home" component={Home} />
            <Scene { ...headerConfig }key="chat" component={Chat} />
         </Scene>
      </Router>
   )
}

export default Routes