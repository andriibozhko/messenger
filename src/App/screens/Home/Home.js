import React from 'react';
import { View, StyleSheet } from 'react-native';
import Chats from '../../containers/Chats';

const Home = () => {


  return (
    <View style={styles.container}>
      <Chats />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#F2F2F2',
  }
})

export default Home;