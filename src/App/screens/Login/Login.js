import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Login = () => {

  const goHome = () => Actions.home();

  return (
    <View style={styles.container}>
      <LinearGradient start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }} colors={['#00BAFF', '#0068FF']} style={styles.header}>
        <Text style={styles.title}>Welcome</Text>
        <Text style={styles.subTitle}>to Messenger</Text>        
      </LinearGradient>
      <TouchableOpacity onPress={goHome} style={styles.btn}><Text style={styles.btnText}>Enter</Text></TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: '100%',
  },
  header: {
    width: '100%',
    height: '50%'
  },
  subTitle: {
    fontWeight: 'bold',
    fontSize: 36,
    color: 'white',
    marginLeft: wp('30%'),
  },
  title: {
    fontSize: 26,    
    color: 'white',
    marginTop: hp('35%'),
    marginLeft: wp('63%'),
  },
  btn: {
    width: 141,
    height: 62,
    backgroundColor: 'white',
    borderRadius: 27,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: '45%'
  },
  btnText: {
    fontWeight: 'bold'
  }
})

export default Login;