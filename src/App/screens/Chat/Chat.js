import React, { useEffect, useState } from 'react';
import { View, ScrollView, StyleSheet, TextInput, KeyboardAvoidingView, TouchableOpacity, Text, Image } from 'react-native'
import { Actions } from 'react-native-router-flux';
import Message from '../../components/Message/index'

const Chat = ({ navigation }) => {
  const messagesConfig = [
    {
      type: 'incoming',
      message: 'Hello! :)',
    },
    {
      type: 'outcoming',
      message: 'Whats up?',
    },
  ]
  
  const { state: { params: { name, surname } } } = navigation;
  const [messages, setNewMessages] = useState(messagesConfig)
  const [value, setNewValue] = useState('');

  useEffect(() => {
    Actions.refresh({ title: `${name} ${surname}` })
  }, []);

  const handlePress = (text) => {
    setNewValue(text);
  }

  const sendNewMessage = () => {
    if(!value.length) {
      return
    }
    const message = {
      type: 'incoming',
      message: value,
    }
  
    setNewMessages([...messages,message]) 
    setNewValue('');   
  }

  return (
    <KeyboardAvoidingView 
      style={styles.container} 
      behavior="padding" 
    >
      <ScrollView contentContainerStyle={styles.messagesContainer}>
        {messages.map((message) => (
          <Message {...message}/>
        ))}
      </ScrollView>
      <View style={styles.inputContainer}>
        <TextInput value={value} onChangeText={handlePress} style={styles.input} placeholder='Type something…'/>
        <TouchableOpacity onPress={sendNewMessage}>
          <Image style={styles.img} source={require('../../../assets/images/SendButton.png')}/>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '90%',
    alignSelf: 'center',
    justifyContent: 'space-between',    
  },
  messagesContainer: {
    height: '97%',
    justifyContent: 'flex-end',
  },
  inputContainer: {
    marginBottom: '5%',
    width: '100%',
    height: 65,
    backgroundColor: '#FFFFFF',
    borderRadius: 32.5,
    flexDirection: 'row',
    justifyContent: 'space-between',    
  },
  input: {
    marginLeft: 30,
    height: 68,
    width: '70%',
  },
  img: {
    marginTop: 5
  }
})

export default Chat;