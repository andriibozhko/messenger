import { combineReducers } from 'redux';
import chats from '../containers/Chats/reducer'

const appReducer = combineReducers({
  chats
});

export default appReducer;