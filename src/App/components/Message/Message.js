import React from 'react';
import { View, Text, StyleSheet } from 'react-native'

const Message = ({ type, message }) => {
  return (
    type === 'incoming' ? (
      <View style={styles.containerOutComing}>
        <Text>{ message }</Text>
      </View>
    ) : (
      <View style={styles.container}>
      <Text style={styles.text}>{ message }</Text>
    </View>
   )
  )
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'flex-start',
    width: 206,
    minHeight: 35,
    backgroundColor: '#0068FF',
    borderRadius: 10,
    padding: 10,
    marginBottom: 16
  },
  containerOutComing: {
    alignSelf: 'flex-end',
    width: 206,
    minHeight: 35,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    marginBottom: 16
  },
  text: {
    color: 'white',
  }
})

export default Message;