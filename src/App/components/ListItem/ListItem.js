import * as React from 'react';
import { View, Text,Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

const ListItem = ({ user }) => {
  const { name, surname } = user;

  const goToChat = () => Actions.chat(user)

  return (
    <TouchableOpacity onPress={goToChat} style={styles.container}>
      <Image style={styles.img} source={require('../../../assets/images/user.png')}/>
      <View style={styles.info}>
        <View>
          <Text style={styles.title}>{name} {surname}</Text>
        </View>
        <View style={styles.messages}>
          <Text>Hello :)</Text>
        </View>
      </View>
      <View>
        <Text style={styles.time}>
          9.22pm
        </Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 89,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  img: {
    height: 40,
    width: 40,
    marginLeft: 24
  },
  time: {
    marginRight: 24,
    color: 'gray'
  },
  info: {
    justifyContent: 'flex-start',
    marginRight: '25%',
    marginLeft: 12,
    width: 150,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18
  },
  messages: {
    marginTop: '3%',
    marginLeft: '2%',
  }
})

export default ListItem;